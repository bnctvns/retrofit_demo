package com.example.makemac.retrofitdemo.helper;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;

/**
 * Created by bonioctavianus on 9/6/16.
 */
public class MyFile {
    public static final String TAG = MyFile.class.getSimpleName();

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();

        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public static File getFileForCacheDirectory(Context context) {
        File directory = context.getCacheDir();

        return directory;
    }
}
