package com.example.makemac.retrofitdemo.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.makemac.retrofitdemo.R;
import com.example.makemac.retrofitdemo.adapter.MoviesAdapter;
import com.example.makemac.retrofitdemo.model.Movie;
import com.example.makemac.retrofitdemo.model.MovieResponse;
import com.example.makemac.retrofitdemo.rest.ApiClient;
import com.example.makemac.retrofitdemo.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private MoviesAdapter moviesAdapter;

    private boolean isRefreshing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initSwipeRefresh();
        createNetworkRequest();
    }

    private void initView() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initSwipeRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isRefreshing) {
                    createNetworkRequest();
                }
            }
        });
    }

    private void hideSwipeRefresh() {
        isRefreshing = false;
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void displayMovies(List<Movie> movies) {
        hideSwipeRefresh();
        moviesAdapter = new MoviesAdapter(movies, R.layout.row_list_movie);
        recyclerView.setAdapter(moviesAdapter);
    }

    private void clearMovies() {
        if (moviesAdapter != null) {
            moviesAdapter.swapMovies(new ArrayList<Movie>());
            moviesAdapter.notifyDataSetChanged();
        }
    }

    private void retryNetworkRequest() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.msg_no_network)
                .setCancelable(false)
                .setPositiveButton(R.string.msg_retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        createNetworkRequest();
                    }
                })

                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        hideSwipeRefresh();
                    }
                })

                .create().show();
    }

    private void createNetworkRequest() {
        isRefreshing = true;
        ApiInterface apiService = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class);
        Call<MovieResponse> call = apiService.getTopRatedMovies(getString(R.string.tmdb_api_key));
        call.enqueue(new CallbackResponseHandler());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_clear) {
            clearMovies();
        }

        return super.onOptionsItemSelected(item);
    }

    private class CallbackResponseHandler implements Callback<MovieResponse> {

        @Override
        public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
            int statusCode = response.code();
            Log.d(TAG, "status code : " + statusCode);
            List<Movie> movies = response.body().getResults();
            displayMovies(movies);
        }

        @Override
        public void onFailure(Call<MovieResponse> call, Throwable t) {
            Log.e(TAG, "Error On Network Request : " + t);
            retryNetworkRequest();
        }
    }
}
