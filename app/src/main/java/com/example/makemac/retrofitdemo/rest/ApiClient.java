package com.example.makemac.retrofitdemo.rest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.makemac.retrofitdemo.helper.MyFile;

import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by makemac on 8/29/16.
 */
public class ApiClient {
    public static final String TAG = ApiClient.class.getSimpleName();

    public static final String BASE_URL = "http://api.themoviedb.org/3/";

    private static Retrofit retrofit = null;

    public static synchronized Retrofit getClient(Context context) {
        if (retrofit == null) {

            if (MyFile.isExternalStorageWritable()) {
                // add OkHttp client for caching purposes
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(getOkHttpClient(context))
                        .build();

            } else {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
        }

        return retrofit;
    }

    public static OkHttpClient getOkHttpClient(final Context context) {
        if (MyFile.isExternalStorageWritable()) {

            OkHttpClient okHttpClient = new OkHttpClient()
                    .newBuilder()
                    .cache(new Cache(MyFile.getFileForCacheDirectory(context), 10 * 1024 * 1024))
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request request = chain.request();
                            if (checkIfHasNetwork(context)) {
                                request = request.newBuilder()
                                        .header("Cache-Control", "public, max-age=" + 60).build(); // 60 seconds

                            } else {
                                Log.e(TAG, "network is unavailable..");
                                request = request.newBuilder()
                                        .header("Cache-Control", "public, only-if-cached, max-stale="
                                                + 60 * 60 * 24 * 7).build(); // 7 days
                            }

                            return chain.proceed(request);
                        }
                    })

                    .build();

            return okHttpClient;
        }

        return null;
    }

    public static boolean checkIfHasNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }
}
